var express = require('express')
var axios = require('axios')
var pug = require("pug")

var app = express()

app.use('/static', express.static('static'))

app.get('/', async (req, res) => {
    const {data: set} = await axios("https://gifstv.com/api/video/reaction/newfeed/1")
    set.forEach((item, i) => set[i].elId = i)
    res.send(pug.renderFile("index.pug", set))
})

app.listen(3000, () =>
  console.log('Example app listening on port 3000!'))
